package com.cosiom.belax.ejercicio1;

import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;


/**
 * Unit test for simple App.
 */
public class AppTest 
{
	WebDriver driver;
	ContactPage cp;
	@Test
    public void testpage(){
		//cambiar ruta del driver
		System.setProperty("webdriver.gecko.driver", "C:\\tools\\geckodriver-v0.18.0-win64\\geckodriver.exe");
		driver= new FirefoxDriver();
		//driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
    	driver.get("https://www.website.com/contact-us/");
    	cp= new ContactPage(driver);
    	insertartexto(cp.casenumber, "55645641");
    	insertartexto(cp.email, "miguelmac20test@gmail.com");
    }
	
	public void insertartexto(WebElement textboxid,String mytexto){
		textboxid.sendKeys(mytexto);
		Logger.getGlobal().info(textboxid+"->"+mytexto);
	}
}
