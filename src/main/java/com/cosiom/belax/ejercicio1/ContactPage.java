package com.cosiom.belax.ejercicio1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ContactPage {
	
	WebDriver driver;
	
	public ContactPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(id="case-number-input")
	WebElement casenumber;
	
	@FindBy(id="case-email-input")
	WebElement email;

	public WebElement gettextbox(String txtid){
		return driver.findElement(By.id(txtid));
	}

}
